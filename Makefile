TEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error
TARGET = protractor-circular-9inch.pdf

%.pdf: %.tex 
	$(TEX) $<

.PHONY: all read clean

all: $(TARGET)

read: $(TARGET)
	evince ${TARGET} > /dev/null 2>&1 &

clean:
	rm -f ${TARGET} *.log *.aux
